const socket = io.connect('http://localhost:3000');
      sendButton = document.querySelector('.send');

sendButton.addEventListener('click', (event) => {
    event.preventDefault();
    let text = document.querySelector('.message').value;
    socket.emit('message', text);
    document.querySelector('.message').value = '';
});

socket.on('new message', (data, username) => {
    const messageContainer = document.createElement('div');
          containerItem = document.createElement('div');
          containerInfo = document.createElement('div');
          author = document.createElement('span');
          time = document.createElement('span');
          content = document.createElement('p');
          ticketContainer = document.querySelector('.messages');
          date = new Date().toLocaleString();  
    messageContainer.classList.add('message-container');
    containerItem.classList.add('message-container-item');
    containerInfo.classList.add('message-info');

    author.textContent = `${username}`;
    time.textContent = `${date.slice(0,8)}`;
    content.textContent = `${data}`;

    
    messageContainer.appendChild(containerItem);
    containerItem.appendChild(containerInfo);
    containerInfo.appendChild(author);
    containerInfo.appendChild(time);
    containerItem.appendChild(content);

    fetchMessage(`${window.location.href}`, [username, data, date], 'POST');
    ticketContainer.insertBefore(messageContainer, document.querySelector('.message-form'));                
    console.log(data, username);
});

function fetchMessage(url, msgArr, method) {
    fetch(url, {
        method: method, credentials: 'include', headers: { 'Content-Type': 'application/json' }, 
        body: JSON.stringify(messageToObject(msgArr))
    })
        .catch(err => console.log(err.message))
}

function messageToObject(msg) {
    const author = arrayCopyAndSpliceToString(msg, 0, 1);
            message = arrayCopyAndSpliceToString(msg, 1, 1);
            time = arrayCopyAndSpliceToString(msg, 2, 2);
            msgObject = {
                author: author,
                message: message,
                time: time
            };

    return msgObject;
}


function arrayCopyAndSpliceToString(arr, start, end) {
    return arr.slice().splice(start, end).join(' ');
}

