window.addEventListener('load', fetchTicketStats('http://localhost:3000/lobby/ticketstats', 'GET'));

function fetchTicketStats(url, method) {
    fetch(url, { method: method , credentials: 'include'})
        .then(res => res.json())
        .then(tickets => ticketStatsCounter(tickets))
        .catch(err => console.log(err))
}


function ticketStatsCounter(tickets) {
    let open = 0;
        inWork = 0;
        completed = 0;

    if (!tickets.length) {
        document.getElementById('open').textContent = open;
        document.getElementById('inWork').textContent = inWork;
        document.getElementById('completed').textContent = completed;
        document.getElementById('total').textContent = tickets.length;

        return;    
    }

    tickets.forEach(ticket => {
        if (ticket.status === 'Открыта') open++;
        if (ticket.status === 'В работе') inWork++;
        if (ticket.status === 'Выполнена') completed++;
    });

    document.getElementById('open').textContent = open;
    document.getElementById('inWork').textContent = inWork;
    document.getElementById('completed').textContent = completed;
    document.getElementById('total').textContent = tickets.length;
    return;
}