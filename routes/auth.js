const express = require('express');
      router = express.Router();
      database = require('../databasequeries/queries');
      
      
      
router.get('/register', (req, res) => res.render('registration'));

router.post('/register', registration);

router.post('/login', login);

router.get('/logout', logout);



function login(req, res) {
    const body = req.body;
          email = body.email.toUpperCase();
    database.checkUserExists(email, userLogin(req, res, email, body.password));
}

function userLogin(req, res, email, pass) {
    return (err, results) => {
        if (err) {
            sendError(req, res, err, 'Ошибка запроса к БД!');
            return;
        };

        if (results.length > 0) {
            const user = results[0];
            if (req.body.password === user.password) {
                createSession(req, res, user);
                req.session.save(err => req.session.reload(err => res.redirect('../lobby')));
            } else {
                res.render('login', { flashError: 'Введите верный пароль!' });
            };
        } else {
            res.render('login', { flashError: 'Такого пользователя не существует!' });
        }
    };
}

function createSession(req, res, user) {
    const session = req.session;
    session.loggedIn = true;
    session.username = `${user.firstName} ${user.lastName}`;
    session.email = user.email;
    session.userId = user.id;
    session.isAdmin = user.isAdmin;
    return session;
}

function registration(req, res) {
    const body = req.body;
          email = body.email.toUpperCase();
    if (body.password === body.retypePassword) {
        database.checkUserExists(email, userRegister(req, res, email, body.firstName, body.lastName, body.password));
    } else {
        res.render('registration', { flashError: 'Введенные пароли не совпадают!' });
    }
}

function userRegister(req, res, email, firstName, lastName, password) {
    return (err, results) => {

        if (err) {
            sendError(req, res, err, 'Ошибка запроса к БД!');
            return;
        };

        if (results.length > 0) {
            const flashError = { flashError: 'Такой пользователь уже существует!' };
            res.render('registration', flashError);

        } else {
            database.createNewUser(email, firstName, lastName, password, createUserInfo(req, res));
        };
    };
};

function createUserInfo(req, res) {
    return function (err, results) {
        if (err) {
            sendError(req, res, err, 'Не удалось зарегистрировать пользователя!');
        }

        const flashInfo = { flashInfo: 'Регистрация завершена! Авторизуйтесь!' };
        res.render('login', flashInfo);
    };
};

function logout(req, res) {
    req.session.destroy((err) => {
        if (err) {
            sendError(req, res, err, 'Не удалось завершить сессию!');
            return;
        };

        res.locals.loggedin = false;
        res.redirect('/');
    });
}

function sendError(req, res, err, message) {
    console.log(err);
    res.status(500).send(message);
}


function loginCheckMiddleWare(req, res, next) {
    if (req.session.loggedIn) {
        next()
    } else {
        res.status(302).send('Вы должны пройти авторизацию для просмотра этой страницы!');
    };
};


function templateIsAdminChecker(req, res, next) {
    res.locals.isAdmin = req.session.isAdmin || null;
    next();
}

exports.templateIsAdminChecker = templateIsAdminChecker;
exports.loginCheckMiddleWare = loginCheckMiddleWare;
exports.sendError = sendError;
exports.router = router;