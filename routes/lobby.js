const express = require('express'); 
      router = express.Router();
      database = require('../databasequeries/queries');
      sendError = require('./auth').sendError;
      multer = require('multer');
      upload = multer({dest:'public/uploads'});  
      loginCheck = require('./auth').loginCheckMiddleWare;
      
      
router.use(loginCheck);

router.get('/', lobbyRender);

router.get('/newticket', (req, res) => res.render('newticket'));

router.post('/newticket', upload.array('images', 3),  (req, res, next) => {
    database.createNewTicket(req.body.title, req.body.content, req.session.userId, (err) => {
        if (err) return console.log(err);

        database.getId(req.session.userId, (err, results) => {
            if (err) return console.log(err);
            
            const files = req.files;
            files.forEach(file => database.insertFiles(file.path, results[0].id, (err) => {
                if (err) return console.log(err);
        
                return console.log('inserted');
            }));
    
            res.redirect('/')
        });
    });
});

router.get('/ticketstats', (req, res) => {
    database.getAllTickets((err, results) => {
        if (err) {
            sendError(req, res, err, 'Ошибка запроса к БД!');
            return;
        }

        res.send(JSON.stringify(results));
    });
});



router.get('/ticket/:id', (req, res) => {
    const id = req.params.id;
    database.getTicket(id, (err, results) => {
        if (err) {
            sendError(req, res, err, 'Ошибка запроса к БД!');
            return;
        }
        
        dateToLocaleString(results);
        let ticket = results;
        database.getFiles(id, (err, results) => {
            if (err) {
                sendError(req, res, err, 'Ошибка запроса к БД!');
                return;
            }

            let files = results;
            database.getMessages(id, (err, results) => {
                if (err) {
                    sendError(req, res, err, 'Ошибка запроса к БД!');
                    return;
                }
            
                let messages = results;
                res.render('ticket', { tickets: ticket, files: files, messages: messages});

            });
        });
    });
}); 


router.post('/ticket/:id', (req, res) => {
    const body = req.body;
          id = req.params.id;
    database.insertMessage(body.message, body.author, body.time.slice(0,8), id, (err) => {
        if (err) {
            sendError(req, res, 'Ошибка запроса к БД!');
            return;
        }

        res.status(200).send('OK');
    });

    console.log(req.body);
});


router.post('/ticket/:id/status', (req, res) => {
    database.updateTicketStatus(req.body.ticketStatus, req.params.id, (err, results) => {
        if (err) {
            sendError(req, res, 'Ошибка запроса к БД!');
            return;
        }

        res.redirect(`./../${req.params.id}`);
    });
});


function lobbyRender(req, res) {
    if (!req.session.isAdmin) {
        return database.getUserTickets(req.session.userId, lobbyPageRender(req, res, 'lobbyuser'));
        
    }

    return database.getAllTickets(lobbyPageRender(req, res, 'lobbyadmin'));
}


function lobbyPageRender(req, res, page) {
    return (err, results) => {
        if (err) {
            sendError(req, res, 'Ошибка запроса к БД!');
            return;
        }
        
        dateToLocaleString(results);
        res.render(page, {tickets: results});
    };
}


function createNewTicket(req, res) {
    const title = req.body.title;
          content = req.body.content;
          author = req.session.userId;
    database.createNewTicket(title, content, author, insertNewTicket(req,res));
}



function insertNewTicket(req, res) {
    return (err, results) => {
        if (err) {
            sendError(req, res, err, 'Ошибка запроса к БД!');
            return;
        }
        
        console.log('Inserted');
        return;
    }
}


function dateToLocaleString(data) {
    data.forEach(elem => {
        const date = elem.date.toLocaleString();
        elem.date = date;
    });

    return;
}




exports.router = router;