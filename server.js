const express = require('express');
      path = require('path');
      app = express();
      authRouter = require('./routes/auth').router;
      lobbyRouter = require('./routes/lobby').router;
      session = require('express-session');
      MySQLStore = require('express-mysql-session')(session);
      bodyParser = require('body-parser');
      server = require('http').Server(app);
      io = require('socket.io')(server);
      templateIsAdminChecker = require('./routes/auth').templateIsAdminChecker;

const MySQLStoreOptions = {
    host: 'localhost',
    port: 3306,
    user: 'voidx',
    password: 'neverhood91289',
    database: 'ticketapp',
    expiration: 9000000000,
    connectionLimit: 10
};

const sessionStore = new MySQLStore(MySQLStoreOptions);

const sessionInfo = session({
    secret: 'fkjghgkjhngkh',
    store: sessionStore,
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 9000000000 }
});



app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(sessionInfo);
app.set('view engine', 'pug');
app.set('views', './views');

app.get('*', templateIsAdminChecker);
app.get('/', mainPageRouing);
app.use('/auth', authRouter);
app.use('/lobby', lobbyRouter);

io.use(function(socket, next) {
    sessionInfo(socket.request, socket.request.res, next);
});

io.on('connection', (socket) => {
    socket.on('message', (data, username) => {
        io.emit('new message', data, socket.request.session.username);
    });
});

server.listen(3000, () => console.log('Server online!'));

function mainPageRouing(req, res) {
    const session = req.session;
    if (session.loggedIn) {
        res.redirect('./lobby');
    } else {
        res.render('login');
    }
}