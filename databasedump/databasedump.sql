-- MySQL dump 10.13  Distrib 8.0.3-rc, for Linux (x86_64)
--
-- Host: localhost    Database: ticketapp
-- ------------------------------------------------------
-- Server version	8.0.3-rc-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `files` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL DEFAULT '',
  `ticket` bigint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `TICKET` (`ticket`),
  CONSTRAINT `TICKET` FOREIGN KEY (`ticket`) REFERENCES `tickets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (1,'public/uploads/baf56c407bf7b46b70dd01321d231bd8',16);
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `messages` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `author` varchar(255) NOT NULL DEFAULT '',
  `time` varchar(255) NOT NULL DEFAULT '',
  `ticket` bigint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `TICKET_ID` (`ticket`),
  CONSTRAINT `TICKET_ID` FOREIGN KEY (`ticket`) REFERENCES `tickets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,'sdsdsd','Сергей Харитонов:','07.03.2018, 22:16:02',4),(2,'ddfdfdfdfdff','Сергей Харитонов:','07.03.2018, 23:27:49',4),(3,'dfdfdfdff','Сергей Харитонов:','07.03.2018, 23:27:51',4),(4,'sdsdsdsdsd','Сергей Харитонов:','07.03.2018, 23:42:46',4),(5,'sdsdsdsdsd','Сергей Харитонов:','07.03.2018, 23:42:46',4),(6,'sdsdsdsdsd','Сергей Харитонов:','07.03.2018, 23:42:46',4),(7,'sdsdsdsdsd','Сергей Харитонов:','07.03.2018, 23:42:56',4),(8,'sdsdsdsdsd','Сергей Харитонов:','07.03.2018, 23:42:56',4),(9,'sdsdsdsdsd','Сергей Харитонов:','07.03.2018, 23:42:56',4),(10,'lol','Сергей Харитонов:','08.03.2018, 21:08:26',4),(11,'sdsdsdsd','Сергей Харитонов:','08.03.2018, 21:08:28',4),(12,'sdsdsdsds','Сергей Харитонов:','08.03.2018, 21:08:30',4),(13,'xzcxcxcxcxc','Сергей Харитонов:','08.03.2018, 21:20:28',2),(14,'ывывывывы','Саша Козлов:','09.03.2018, 1:02:58',3),(15,'Привет','Сергей Харитонов:','09.03.2018, 15:40:41',3),(16,'Привет','Саша Козлов:','09.03.2018, 15:40:57',3),(17,'Привет','Саша Козлов:','09.03.2018, 15:40:57',3),(18,'ghbdtn','Саша Козлов:','09.03.2018, 16:00:13',3),(19,'ывывывы','Саша Козлов:','13.03.2018, 15:50:54',8),(20,'sadsdsds','Саша Козлов:','13.03.2018, 16:16:31',16),(21,'sdsdsd','Сергей Харитонов:','14.03.2018, 21:38:38',4),(22,'sdsdsdssssssaaaa','Сергей Харитонов:','14.03.2018, 21:38:57',4),(23,'ghjjkjjkljklkl','Сергей Харитонов:','14.03.2018, 21:39:36',4),(24,'sdsdsdsds','Сергей Харитонов:','14.03.2018, 21:41:42',1),(25,'drdrere','Сергей Харитонов:','15.03.2018, 0:02:24',1);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('XX0PvVqW5oPQM-d8Y_v834vqqn1k4yeV',1529599349,'{\"cookie\":{\"originalMaxAge\":9000000000,\"expires\":\"2018-06-21T16:40:50.247Z\",\"httpOnly\":true,\"path\":\"/\"},\"loggedIn\":true,\"username\":\"Саша Козлов\",\"email\":\"LOL@MAIL.RU\",\"userId\":3,\"isAdmin\":0}'),('pn2ggyDxENSrqP4FBBRFRxd9Dg61BpFU',1530061421,'{\"cookie\":{\"originalMaxAge\":9000000000,\"expires\":\"2018-06-26T22:14:55.013Z\",\"httpOnly\":true,\"path\":\"/\"},\"loggedIn\":true,\"username\":\"Сергей Харитонов\",\"email\":\"VOIDEX@MAIL.RU\",\"userId\":1,\"isAdmin\":1}'),('qYkrTOzONIkdHW1xYrqdd0TlvQ4sgFDJ',1529612104,'{\"cookie\":{\"originalMaxAge\":9000000000,\"expires\":\"2018-06-21T17:00:06.594Z\",\"httpOnly\":true,\"path\":\"/\"},\"loggedIn\":true,\"username\":\"Саша Козлов\",\"email\":\"LOL@MAIL.RU\",\"userId\":3,\"isAdmin\":0}'),('w6z6XJZJux6D-mYT9houEqICDNZgIzIx',1529546586,'{\"cookie\":{\"originalMaxAge\":9000000000,\"expires\":\"2018-06-21T01:55:53.160Z\",\"httpOnly\":true,\"path\":\"/\"},\"loggedIn\":true,\"username\":\"Саша Козлов\",\"email\":\"LOL@MAIL.RU\",\"userId\":3,\"isAdmin\":0}');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tickets` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `author` bigint(11) unsigned NOT NULL,
  `date` datetime DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Открыта',
  PRIMARY KEY (`id`),
  KEY `POST_AUTHOR` (`author`),
  CONSTRAINT `POST_AUTHOR` FOREIGN KEY (`author`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` VALUES (1,'Test','sdsdsds',1,'2018-03-01 21:34:28','В работе'),(2,'Test','ывывыв',1,'2018-03-01 23:34:23','Выполнена'),(3,'test','sdadsdaa',3,'2018-03-03 15:13:19','В работе'),(4,'Not working','TEST',2,'2018-03-05 16:37:49','Выполнена'),(5,'sdsd','ssdsdsds',3,'2018-03-10 14:37:54','В работе'),(6,'another test','test',3,'2018-03-10 14:38:36','Открыта'),(7,'another test','test',3,'2018-03-10 14:38:42','Открыта'),(8,'and one more','sdsdsd',3,'2018-03-10 14:42:58','Открыта'),(9,'and one more','sdsdsd',3,'2018-03-10 14:44:58','Открыта'),(10,'sdsdsdssssss','sssssssss',3,'2018-03-10 14:49:02','Открыта'),(11,'ssss','ssss',3,'2018-03-10 14:49:52','Открыта'),(12,'ssdsdsd','sdsdsdsdsolololol',3,'2018-03-10 14:57:11','Открыта'),(13,'sdsdsdsd','sdsdsdsdsdsssssssss',3,'2018-03-10 14:57:48','Открыта'),(14,'aaaaaaa','aaaaaaaaaaaa',3,'2018-03-10 15:00:33','Открыта'),(15,'ffffffffffff','fffffffffff',3,'2018-03-10 16:01:40','Открыта'),(16,'ssssssssss','ssssssssssssssss',3,'2018-03-10 16:01:53','Открыта'),(17,'sdsdsdsds','sdsdsdsdsd',3,'2018-03-13 17:31:08','Открыта'),(18,'sdsdsdsd','sdadasdadasdafdsadas',3,'2018-03-13 17:31:15','Открыта'),(19,'ssssssssss','sssaaaaaaaaaaaaaaaaaaaaaaaaa',3,'2018-03-13 17:32:05','Открыта');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL DEFAULT '',
  `lastName` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` text NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_unq` (`email`),
  KEY `email_idx` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Сергей','Харитонов','VOIDEX@MAIL.RU','blizz633991',1),(2,'Сергей','Харитонов','SOLAR91289@MAIL.RU','12345',0),(3,'Саша','Козлов','LOL@MAIL.RU','123',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-16 20:19:53
