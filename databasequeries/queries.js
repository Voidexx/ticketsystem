const mysql = require('mysql');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'voidx',
    password: 'neverhood91289',
    database: 'ticketapp',
    connectionLimit: 20,
    supportBigNumbers: true,
});

exports.getId = (author, callback) => {
    const query = 'SELECT id , title, content FROM tickets WHERE author = ? ORDER BY id DESC LIMIT 1;';

    dataBaseQuery(query, [author], callback);
};

exports.getFiles = (ticket, callback) => {
    const query = 'SELECT path FROM files WHERE ticket = ?;';

    dataBaseQuery(query, [ticket], callback);
};

exports.insertFiles = (path, ticketId, callback) => {
    const query = 'INSERT INTO files(path, ticket) VALUES(?, ?);';

    dataBaseQuery(query, [path, ticketId], callback);
}

exports.checkUserExists = (email, callback) => {
    const query = 'SELECT * FROM users u WHERE u.email = ?;';

    dataBaseQuery(query, [email], callback);
};

exports.createNewUser = (email, firstName, lastName, password, callback) => {
    const query = 'INSERT INTO users(email, firstName, lastName, password) VALUES(?, ?, ?, ?);';

    dataBaseQuery(query, [email, firstName, lastName, password], callback);
}

exports.createNewTicket = (title, content, author, callback) => {
    const query = 'INSERT INTO tickets(title, content, date, author) VALUES(?, ?, NOW(), ?);';

    dataBaseQuery(query, [title, content, author], callback);
};

exports.updateTicketStatus = (status, id, callback) => {
    const query = 'UPDATE tickets SET status = ? WHERE id = ?;';

    dataBaseQuery(query, [status, id], callback);
}

exports.getTicket = (id, callback) => {
    const query = 'SELECT u.email, u.firstName, u.lastName, t.title, t.content, t.date, t.status, t.id FROM tickets t INNER JOIN users u ON t.author = u.id WHERE t.id = ?';

    dataBaseQuery(query, [id], callback);
 }

 exports.getMessages = (id, callback) => {
    const query = 'SELECT message, author, time FROM messages WHERE ticket = ?';

    dataBaseQuery(query, [id], callback);
 }

exports.getUserTickets = (userId, callback) => {
    const query = 'SELECT u.email, u.firstName, u.lastName, t.title, t.content, t.date, t.status, t.id FROM tickets t INNER JOIN users u ON  t.author = u.id WHERE u.id = ? ORDER BY date DESC;';

    dataBaseQuery(query , [userId], callback);
};


exports.getAllTickets = (callback) => {
    const query = 'SELECT u.email, u.firstName, u.lastName, t.title, t.content, t.date, t.status, t.id FROM tickets t INNER JOIN users u ON t.author = u.id ORDER BY date;';

    dataBaseQuery(query, [], callback);
}

exports.insertMessage = (message, author, time, ticket, callback) => {
    const query = 'INSERT INTO messages(message, author, time, ticket) VALUES(?,?,?,?);';

    dataBaseQuery(query, [message, author, time, ticket], callback);
}

function dataBaseQuery(query, data, callback) {
    pool.getConnection((err, connection) => {
        if (err) {
            console.log(err);
            callback(err);
            return;
        }

        connection.query(query, data, (err, results) => {
            connection.release();
            if (err) {
                console.log(err);
                callback(err);
                return;
            }
            
            callback(null, results);
        });
    });
}

